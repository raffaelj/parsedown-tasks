<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class ParsedownTasksTest extends TestCase
{
    const MARKDOWN = <<<EOF
Test

- [ ] test 1
- [] test 2
- [x] test 3
    - [ ] test 4 <a></a>
    - [x] test 5
- [test](https://markdown.org) 6
- [x](https://markdown.org) test 7
- [x] [test 8](https://markdown.org)
- [ ] **test 9**

[ ] test 10

[x] test 11

- [ ] test [x] 12
- [x] test [ ] 13
- [x] test **[ ] 14**
- [x] test [[ ] ](https://markdown.org) 15
EOF;

    const HTML = <<<EOF
<p>Test</p>
<ul>
<li>
<input type="checkbox" disabled /> test 1
</li>
<li>[] test 2</li>
<li>
<input type="checkbox" disabled checked /> test 3
<ul>
<li>
<input type="checkbox" disabled /> test 4 <a></a>
</li>
<li>
<input type="checkbox" disabled checked /> test 5
</li>
</ul>
</li>
<li><a href="https://markdown.org">test</a> 6</li>
<li><a href="https://markdown.org">x</a> test 7</li>
<li>
<input type="checkbox" disabled checked /> <a href="https://markdown.org">test 8</a>
</li>
<li>
<input type="checkbox" disabled /> <strong>test 9</strong>
</li>
</ul>
<p>[ ] test 10</p>
<p>[x] test 11</p>
<ul>
<li>
<input type="checkbox" disabled /> test [x] 12
</li>
<li>
<input type="checkbox" disabled checked /> test [ ] 13
</li>
<li>
<input type="checkbox" disabled checked /> test <strong>[ ] 14</strong>
</li>
<li>
<input type="checkbox" disabled checked /> test <a href="https://markdown.org">[ ] </a> 15
</li>
</ul>
EOF;

    const HTML_WITH_CLASSES = <<<EOF
<p>Test</p>
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test 1
</li>
<li>[] test 2</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test 3
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test 4 <a></a>
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test 5
</li>
</ul>
</li>
<li><a href="https://markdown.org">test</a> 6</li>
<li><a href="https://markdown.org">x</a> test 7</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> <a href="https://markdown.org">test 8</a>
</li>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> <strong>test 9</strong>
</li>
</ul>
<p>[ ] test 10</p>
<p>[x] test 11</p>
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test [x] 12
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test [ ] 13
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test <strong>[ ] 14</strong>
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test <a href="https://markdown.org">[ ] </a> 15
</li>
</ul>
EOF;

    const HTML_SANITIZED = <<<EOF
<p>Test</p>
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test 1
</li>
<li>[] test 2</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test 3
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test 4 &lt;a&gt;&lt;/a&gt;
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test 5
</li>
</ul>
</li>
<li><a href="https://markdown.org">test</a> 6</li>
<li><a href="https://markdown.org">x</a> test 7</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> <a href="https://markdown.org">test 8</a>
</li>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> <strong>test 9</strong>
</li>
</ul>
<p>[ ] test 10</p>
<p>[x] test 11</p>
<ul>
<li class="parsedown-task-list parsedown-task-list-open">
<input type="checkbox" disabled /> test [x] 12
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test [ ] 13
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test <strong>[ ] 14</strong>
</li>
<li class="parsedown-task-list parsedown-task-list-close">
<input type="checkbox" disabled checked /> test <a href="https://markdown.org">[ ] </a> 15
</li>
</ul>
EOF;

    public function testTasklist()
    {
        $parsedown = new ParsedownTasks();
        $this->assertEquals(self::HTML, $parsedown->text(self::MARKDOWN));
    }

    public function testTasklistWithClasses()
    {
        $options = [
            'classUnchecked' => 'parsedown-task-list parsedown-task-list-open',
            'classChecked'   => 'parsedown-task-list parsedown-task-list-close',
        ];
        $parsedown = new ParsedownTasks($options);
        $this->assertEquals(self::HTML_WITH_CLASSES, $parsedown->text(self::MARKDOWN));
    }

    public function testTasklistWithSafeMode()
    {
        $options = [
            'classUnchecked' => 'parsedown-task-list parsedown-task-list-open',
            'classChecked'   => 'parsedown-task-list parsedown-task-list-close',
        ];
        $parsedown = new ParsedownTasks($options);
        $parsedown->setSafeMode(true);
        $this->assertEquals(self::HTML_SANITIZED, $parsedown->text(self::MARKDOWN));
    }

}
